import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

function Square(props) {
    let squareClass = props.value === 'X' ? 'square-x' : 'square-o';
    let classes = `square ${squareClass}`;

    return (
        <button tabIndex="-1" className={classes} onClick={props.onClick}>
            {props.value}
        </button>
    );
}

function calculateWinner(squares) {
    const lines = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
        const [a, b, c] = lines[i];
        if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
            return squares[a];
        }
    }
    return null;
}

function ActionButton(props) {
    let title = props.status === GameStatus.NotStart ? 'Start Game' : 'Restart Game';

    return (
        <button onClick={props.onClick}>
            {title}
        </button>
    );
}

class Player extends React.Component {

    render() {
        const status = this.props.status;
        let point = this.props.point === '0' ? '-' : this.props.point;
        let playerName, activeClass;
        if (status !== GameStatus.NotStart) {
            playerName = <label className="player-name">{this.props.name}</label>
            activeClass = this.props.isNext ? 'active' : '';
        } else {
            playerName = <input type="text" className="player-name" value={this.props.name}
                placeholder="Enter player's name" onChange={this.props.onChange} />
        }
        
        let typeClass = this.props.type === 'X' ? 'type-x' : 'type-o';
        let classes = `game-player-info ${typeClass} ${activeClass}`;

        return (
            <div className={classes}>
                <label className="player-type">{this.props.type}</label>
                {playerName}
                <label className="player-point">{point}</label>
            </div>
        );
    }
}

class Board extends React.Component {
    renderSquare(i) {
        return <Square value={this.props.squares[i]} onClick={() => this.props.onClick(i)} />;
    }

    render() {
        return (
            <div>
                <div className="board-row">
                    {this.renderSquare(0)}
                    {this.renderSquare(1)}
                    {this.renderSquare(2)}
                </div>
                <div className="board-row">
                    {this.renderSquare(3)}
                    {this.renderSquare(4)}
                    {this.renderSquare(5)}
                </div>
                <div className="board-row">
                    {this.renderSquare(6)}
                    {this.renderSquare(7)}
                    {this.renderSquare(8)}
                </div>
            </div>
        );
    }
}

const GameStatus = Object.freeze({ NotStart: Symbol(), Started: Symbol(), GameOver: Symbol() });
class Game extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            history: [{
                squares: Array(9).fill(null),
            }],
            stepNumber: 0,
            xIsNext: true,
            status: GameStatus.NotStart,
            xPlayer: "",
            xPoint: 0,
            oPlayer: "",
            oPoint: 0
        };

        this.handleInputXPlayer = this.handleInputXPlayer.bind(this);
        this.handleInputOPlayer = this.handleInputOPlayer.bind(this);
    }

    // Handle click on board
    handleClick(i) {
        const status = this.state.status;
        const history = this.state.history.slice(0, this.state.stepNumber + 1);
        const current = history[history.length - 1];
        const squares = current.squares.slice();
        const pointX = this.state.xPoint;
        const pointO = this.state.oPoint;

        if (status !== GameStatus.NotStart && status !== GameStatus.GameOver) {
            let winner = calculateWinner(squares);
            if (winner || squares[i]) {
                return;
            }
            squares[i] = this.state.xIsNext ? 'X' : 'O';
            this.setState({
                history: history.concat([{
                    squares: squares,
                }]),
                stepNumber: history.length,
                xIsNext: !this.state.xIsNext
            });

            // Check winner
            winner = calculateWinner(squares)
            if (!winner) {
                // Check draw
                if (this.state.stepNumber === 9) {
                    // Change game status if game is started
                    if (status === GameStatus.Started) {
                        this.setState({
                            status: GameStatus.GameOver
                        });
                    }
                }
            } else {
                // Check who is winner
                let xIsWin = winner === 'X';
                // Change game status if game is started
                if (status === GameStatus.Started) {
                    this.setState({
                        status: GameStatus.GameOver,
                        xPoint: xIsWin ? pointX + 1 : pointX,
                        oPoint: !xIsWin ? pointO + 1 : pointO
                    });
                }
            }
        }
    }

    // Jumpt to history
    jumpTo(step) {
        const status = this.state.status;
        if (status !== GameStatus.NotStart && status !== GameStatus.GameOver) {
            this.setState({
                stepNumber: step,
                xIsNext: (step % 2) === 0,
            });
        }
    }

    // Handle click Start Button
    handleClickActionButton() {
        const status = this.state.status;
        if (status === GameStatus.NotStart || status === GameStatus.GameOver) {
            this.setState({
                status: GameStatus.Started
            });
        }

        this.setState({
            history: [{
                squares: Array(9).fill(null),
            }],
            stepNumber: 0,
            xIsNext: true,
        });
    }

    // Handle change value of input player X
    handleInputXPlayer(e) {
        this.setState({
            xPlayer: e.target.value
        });
    }

    // Handle change value of input player O
    handleInputOPlayer(e) {
        this.setState({
            oPlayer: e.target.value
        });
    }

    render() {
        const history = this.state.history;
        const current = history[this.state.stepNumber];
        const winner = calculateWinner(current.squares);
        const status = this.state.status;
        const nameX = this.state.xPlayer;
        const pointX = this.state.xPoint;
        const nameO = this.state.oPlayer;
        const pointO = this.state.oPoint;

        const moves = history.map((step, move) => {
            const desc = move ?
                'Go to move #' + move :
                'Go to game start';
            return (
                <li key={move}>
                    <button onClick={() => this.jumpTo(move)}>{desc}</button>
                </li>
            );
        });

        let message;
        if (status !== GameStatus.NotStart) {
            if (!winner) {
                // Check draw
                if (this.state.stepNumber === 9) {
                    message = 'Draw';
                } else {
                    // Get name of next player
                    let playerName;
                    if (this.state.xIsNext) {
                        playerName = nameX;
                    } else {
                        playerName = nameO;
                    }

                    // Show message
                    let nextPlayer = (this.state.xIsNext ? 'X' : 'O');
                    if (playerName.trim() === '') {
                        message = nextPlayer + ' Turn';
                    } else {
                        message = playerName + ' (' + nextPlayer + ') Turn';
                    }
                }
            } else {
                // Check who is winner
                let xIsWin = winner === 'X';

                // Get name of winner
                let playerName;
                if (xIsWin) {
                    playerName = nameX;
                } else {
                    playerName = nameO;
                }

                // Show message
                if (playerName.trim() === '') {
                    message = winner + ' Win!!';
                } else {
                    message = playerName + ' Win!!';
                }
            }
        } else {
            message = 'Please input player\'s names, then press Start Game';
        }

        return (
            <div className="game">
                <div className="game-row">
                    <div className="game-player">
                        <div className="game-col">
                            <Player status={status} isNext={this.state.xIsNext} type="X" name={nameX} point={pointX} onChange={this.handleInputXPlayer} />
                        </div>
                        <div className="game-col ml-10">
                            <Player status={status} isNext={!this.state.xIsNext} type="O" name={nameO} point={pointO} onChange={this.handleInputOPlayer} />
                        </div>
                    </div>
                </div>
                <div className="game-message mt-10">{message}</div>
                <div className="game-row">
                    <div className="game-board-info">
                        <div className="game-board">
                            <Board
                                squares={current.squares}
                                onClick={(i) => this.handleClick(i)}
                            />
                        </div>
                        <div className="game-info">
                            <div>History:</div>
                            <ol>{moves}</ol>
                        </div>
                    </div>
                </div>
                <div className="game-start-button">
                    <ActionButton status={status} onClick={() => this.handleClickActionButton()} />
                </div>
            </div>
        );
    }
}

// ========================================

ReactDOM.render(
    <Game />,
    document.getElementById('root')
);
